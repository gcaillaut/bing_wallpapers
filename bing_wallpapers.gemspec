# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'bing_wallpapers/version'

Gem::Specification.new do |spec|
  spec.name          = "bing_wallpapers"
  spec.version       = BingWallpapers::VERSION
  spec.authors       = ["Gaëtan Caillaut"]
  spec.email         = ["gaetan.caillaut@live.com"]

  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com' to prevent pushes to rubygems.org, or delete to allow pushes to any server."
  end

  spec.summary       = %q{Fetch wallpapers from Bing}
  # spec.description   = %q{TODO: Write a longer description or delete this line.}
  spec.homepage      = "https://bitbucket.org/gcaillaut/bing_wallpapers"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "bin"
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.8"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec"

  spec.add_runtime_dependency "nokogiri"
end
