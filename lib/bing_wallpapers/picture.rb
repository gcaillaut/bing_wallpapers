require 'date'
require 'nokogiri'
require 'open-uri'

module BingWallpapers
  class PictureData
    attr_reader :title, :base_url, :startdate, :enddate

    def initialize(options)
      @title = options[:title]
      @base_url = options[:base_url]
      @startdate = options[:startdate]
      @enddate = options[:enddate]
    end

    def to_s
      "#{@title}"
    end

    def self.load_pictures_data(mkt, n, idx)
      xml_url = "https://bing.com/HPImageArchive.aspx?format=xml&idx=#{idx}&n=#{n}&mkt=#{mkt}"
      xml_content = open(xml_url)
      xml_doc = Nokogiri::XML(xml_content)
      data = BingWallpapers.parse_xml(xml_doc)

      return PictureData.new(data.first) if n == 1
      data.collect { |d| PictureData.new(d) }
    end
  end

  def parse_xml(xml_doc)
    base_xpath = '//images//image'
    labels = [:title, :base_url, :startdate, :enddate]
    res = []

    title = xml_doc.xpath("#{base_xpath}//copyright").collect { |n| n.content }
    base_url = xml_doc.xpath("#{base_xpath}//urlBase").collect { |n| n.content }

    startdate = xml_doc.xpath("#{base_xpath}//startdate").collect do |d|
      Date.strptime(d.content.to_s, '%Y%m%d')
    end

    enddate = xml_doc.xpath("#{base_xpath}//enddate").collect do |d|
      Date.strptime(d.content.to_s, '%Y%m%d')
    end

    title.zip(base_url, startdate, enddate) do |elems|
      res << Hash[labels.zip(elems)]
    end

    return res
  end

  module_function :parse_xml
end
