require 'spec_helper'

RSpec.describe BingWallpapers do

  let(:mkt) { 'fr-FR' }

  context :fetch do
    it 'should fetch a single picture' do
      data = BingWallpapers.fetch(mkt)
      expect(data.class).to be == BingWallpapers::PictureData
    end

    it 'should fetch 3 pictures' do
      data = BingWallpapers.fetch(mkt, 3, 0)
      expect(data.size).to eq 3
    end

    it 'should fetch nil data' do
      data = BingWallpapers.fetch(mkt, 3)
      data.each do |d|
        expect([d.title, d.base_url, d.startdate, d.enddate]).not_to include(nil)
      end
    end

    it 'startdate should be greater than enddate' do
      data = BingWallpapers.fetch(mkt)
      expect(data.startdate).to be < data.enddate
    end
  end

end
